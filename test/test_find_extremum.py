import numpy as np
from scipy.optimize import rosen as rosen_scipy
from emasses import EmassCalculator


def rosen_2d(r, a, b):
    x = r[0]
    y = r[1]
    return (a - x)**2 + b * (y - x**2)**2


def test_find_extremum():
    EMC = EmassCalculator(rosen_2d)
    x0 = np.array([0.2, 0.3])
    a = 1
    b = 100
    x_min = EMC.find_extremum(x0=x0, a=a, b=b)
    assert np.allclose(x_min, np.array([a, a**2]))

    a2 = 2.15
    b2 = 100
    x_min2 = EMC.find_extremum(x0=-x0, a=a2, b=b2)
    assert np.allclose(x_min2, np.array([a2, a2**2]))

    EMC.calculator = rosen_scipy
    x_min3 = EMC.find_extremum(x0=[-13.4, 10.0, 100.0])
    assert np.allclose(x_min3, np.array([1, 1, 1]))
